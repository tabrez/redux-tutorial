import { createSlice } from '@reduxjs/toolkit'

export interface UserState {
  id: string
  name: string
}

const initialState: UserState[] = [{ id: '1', name: 'Mark Erikson' }]

const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {},
})

export const usersReducer = usersSlice.reducer
