import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { fetchPosts, getUserName, selectPosts, Status } from './postsSlice'
import { ReactionButtons } from './reactionButtons'
import { TimeAgo } from './timeAgo'

export const PostsList = () => {
  const dispatch = useAppDispatch()
  const posts = useAppSelector(selectPosts)
  const postStatus = useAppSelector(state => state.posts.status)

  useEffect(() => {
    if (postStatus === Status.Idle) {
      dispatch(fetchPosts())
    }
  }, [])

  // const orderedPosts = posts
  //   .slice()
  //   .sort((a, b) => b.date.localeCompare(a.date))

  return (
    <>
      <table>
        <thead>
          <tr>
            <th>title</th>
            <th>content</th>
            <th>date</th>
          </tr>
        </thead>
        <tbody>
          {posts.map(post => (
            <tr key={post.id}>
              <td>{post.title}</td>
              <td>{post.content.substring(0, 15)}</td>
              <td>
                <TimeAgo timestamp={post.date} />
              </td>
              {/* <td>{getUserName(post.userId)}</td> */}
              <td>
                <ReactionButtons post={post} />
              </td>
              <td>
                <Link to={`/posts/${post.id}`} className="button muted-button">
                  View Post
                </Link>
              </td>
              <td>
                <Link
                  to={`/posts/${post.id}/edit`}
                  className="button muted-button"
                >
                  Edit Post
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}
