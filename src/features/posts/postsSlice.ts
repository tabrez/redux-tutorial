import {
  createAsyncThunk,
  createSlice,
  nanoid,
  PayloadAction,
} from '@reduxjs/toolkit'
import { useAppSelector } from '../../app/hooks'
import { client } from '../../api/client'

export enum Status {
  Idle = 'idle',
  Loading = 'loading',
  Succeeded = 'succeded',
  Failed = 'failed',
}

export interface Reactions {
  thumbsUp: number
  hooray: number
  heart: number
  rocket: number
  eyes: number
}

export interface Post {
  id: string
  date: string
  title: string
  content: string
  userId: string
  reactions: Reactions
}

export interface PostsState {
  items: Post[]
  status: Status
  error: string | null
}

export interface PostReaction {
  postId: string
  reaction: keyof Reactions
}

export const fetchPosts = createAsyncThunk('posts/fetchPosts', async () => {
  const response = await client.get('/fakeApi/posts')
  return response.posts
})

const initialState: PostsState = {
  items: [
    // {
    //   id: nanoid(),
    //   date: sub(new Date(), { minutes: 5 }).toISOString(),
    //   title: 'The C Programming Language',
    //   content: 'haha',
    //   userId: '1',
    //   reactions: {
    //     thumbsUp: 0,
    //     hooray: 0,
    //     heart: 0,
    //     rocket: 0,
    //     eyes: 0,
    //   },
    // },
  ],
  status: Status.Idle,
  error: null,
}

const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    addNewPost: {
      reducer(state, action: PayloadAction<Post>) {
        state.items.push(action.payload)
      },
      prepare(title, content, userId) {
        return {
          payload: {
            id: nanoid(),
            date: new Date().toISOString(),
            title,
            content,
            userId,
            reactions: {
              thumbsUp: 0,
              hooray: 0,
              heart: 0,
              rocket: 0,
              eyes: 0,
            },
          },
        }
      },
    },
    editPost(state, action: PayloadAction<Post>) {
      state.items.find(post => {
        if (post.id === action.payload.id) {
          post.title = action.payload.title
          post.content = action.payload.content
        }
      })
    },
    addReaction(state, action: PayloadAction<PostReaction>) {
      const post = state.items.find(post => post.id === action.payload.postId)
      if (!post) return
      post.reactions[action.payload.reaction]++
    },
  },
  extraReducers: builder => {
    builder.addCase(fetchPosts.pending, state => {
      state.status = Status.Loading
    })
    builder.addCase(fetchPosts.fulfilled, (state, action) => {
      state.status = Status.Succeeded
      state.items = state.items.concat(action.payload)
    })
    builder.addCase(fetchPosts.rejected, (state, action) => {
      state.status = Status.Failed
      state.error = action.error.message ? action.error.message : null
    })
  },
})

export const getUserName = (userId: string) => {
  const users = useAppSelector(state => state.users)
  const user = users.find(user => user.id === userId)
  return user ? user.name : 'N/A'
}

export const { addNewPost, editPost, addReaction } = postsSlice.actions
export const selectPosts = (state: { posts: PostsState }) => state.posts.items
export const selectPostsById = (
  state: { posts: PostsState },
  postId: string,
) => {
  return state.posts.items.find(post => post.id === postId)
}
export const postsReducer = postsSlice.reducer
