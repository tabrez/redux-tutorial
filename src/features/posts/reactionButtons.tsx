import React from 'react'
import { useAppDispatch } from '../../app/hooks'
import { addReaction, Post, Reactions } from './postsSlice'

const reactionEmoji = {
  thumbsUp: '👍',
  hooray: '🎉',
  heart: '❤️',
  rocket: '🚀',
  eyes: '👀',
}

export const ReactionButtons = ({ post }: { post: Post }) => {
  const dispatch = useAppDispatch()
  const reactionButtons = Object.entries(reactionEmoji).map(([name, emoji]) => {
    return (
      <button
        key={name}
        type="button"
        onClick={() =>
          dispatch(
            addReaction({ postId: post.id, reaction: name as keyof Reactions }),
          )
        }
        className="muted-button reaction-button"
      >
        {emoji} {post.reactions[name as keyof Reactions]}
      </button>
    )
  })

  return <div>{reactionButtons}</div>
}
