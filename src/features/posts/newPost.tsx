import React, { useState } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { addNewPost } from './postsSlice'

export const NewPost = () => {
  const [title, setTitle] = useState('')
  const [content, setContent] = useState('')
  const [userId, setUserId] = useState('1')

  const dispatch = useAppDispatch()
  const users = useAppSelector(state => state.users)

  return (
    <>
      <h3>Submit a new post</h3>
      <label htmlFor="postTitle">Title:</label>
      <input
        id="postTitle"
        name="postTitle"
        type="text"
        value={title}
        onChange={e => setTitle(e.target.value)}
      />
      <br />

      <label htmlFor="postContent">Content:</label>
      <textarea
        id="postContent"
        name="postContent"
        value={content}
        onChange={e => setContent(e.target.value)}
      />
      <br />

      <label htmlFor="user">User:</label>
      <select
        id="userId"
        name="userId"
        onChange={e => setUserId(e.target.value)}
      >
        {users.map(user => (
          <option key={user.id} value={user.id}>
            {user.name}
          </option>
        ))}
      </select>

      <button
        type="button"
        onClick={() => {
          dispatch(addNewPost(title, content, userId))
          setTitle('')
          setContent('')
          setUserId('1')
        }}
      >
        Submit new post
      </button>
    </>
  )
}
