import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { editPost, selectPostsById } from './postsSlice'
import { PostMatchType } from './showPost'

export const EditPost = ({ match }: { match: PostMatchType }) => {
  const id = match.params.postId
  const post = useAppSelector(state => selectPostsById(state, id))

  if (!post) {
    return (
      <section>
        <p>Post not found</p>
      </section>
    )
  }

  const [title, setTitle] = useState(post.title)
  const [content, setContent] = useState(post.content)
  const [userId, setUserId] = useState(post.userId)

  const users = useAppSelector(state => state.users)
  const dispatch = useAppDispatch()
  const history = useHistory()

  return (
    <>
      <h3>Edit post</h3>

      <label htmlFor="postTitle">Title:</label>
      <input
        id="postTitle"
        name="postTitle"
        type="text"
        value={title}
        onChange={e => setTitle(e.target.value)}
      />
      <br />

      <label htmlFor="postContent">Content:</label>
      <textarea
        id="postContent"
        name="postContent"
        value={content}
        onChange={e => setContent(e.target.value)}
      />
      <br />

      <label htmlFor="user">User:</label>
      <select
        id="userId"
        name="userId"
        onChange={e => setUserId(e.target.value)}
      >
        {users.map(user => (
          <option key={user.id} value={user.id}>
            {user.name}
          </option>
        ))}
      </select>

      <button
        type="button"
        onClick={() => {
          dispatch(
            editPost({
              id,
              date: post.date,
              title,
              content,
              userId,
              reactions: post.reactions,
            }),
          )
          history.push(`/posts/${id}`)
        }}
      >
        Save post
      </button>
    </>
  )
}
