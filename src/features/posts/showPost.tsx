import React from 'react'
import { useAppSelector } from '../../app/hooks'
import { getUserName, selectPostsById } from './postsSlice'
import { ReactionButtons } from './reactionButtons'

export interface PostMatchType {
  params: {
    postId: string
  }
}

export const ShowPost = ({ match }: { match: PostMatchType }) => {
  const { postId } = match.params

  const post = useAppSelector(state => selectPostsById(state, postId))
  if (!post) {
    return (
      <section>
        <h2>Post not found</h2>
      </section>
    )
  }
  return (
    <section>
      <article className="post">
        <h2>{post.title}</h2>
        <p className="post-content">{post.content}</p>
        <div>{getUserName(post.userId)}</div>
        <br />
        <div>{post.date}</div>
        <br />
        <div>
          <ReactionButtons post={post} />
        </div>
      </article>
    </section>
  )
}
