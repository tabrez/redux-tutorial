import React from 'react'
import { Link } from 'react-router-dom'

export const NavBar = () => {
  return (
    <ul>
      <li>
        <Link to={`/counter/`}>View Counter</Link>
      </li>
      <li>
        <Link to={`/posts/`}>View List of Posts</Link>
      </li>
      <li>
        <Link to={`/new-post/`}>Submit new post</Link>
      </li>
      <br />
      <Link to={`/`}>Home</Link>
    </ul>
  )
}
