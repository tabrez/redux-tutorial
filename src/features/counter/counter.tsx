import React, { useState } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { store } from '../../app/store'
import { logAndAdd, selectCount } from './counterSlice'
import { CounterState, decrement, increment } from './counterSlice'

const selectCounterValue = (state: { counter: CounterState }) =>
  state.counter.value

export const Counter = () => {
  const count = useAppSelector(selectCount)
  const dispatch = useAppDispatch()
  const [delta, setDelta] = useState(0)

  return (
    <div>
      <button
        aria-label="Increment value by 1"
        onClick={() => {
          console.log(`increment: ${increment}`)
          const i = increment()
          console.log(`increment(): ${JSON.stringify(i)}`)
          // dispatch(i)
          store.dispatch(i)
          console.log(`counter value: ${selectCounterValue(store.getState())}`)
        }}
      >
        increment
      </button>
      <span>{count}</span>
      <button
        aria-label="Decrement value by 1"
        onClick={() => dispatch(decrement())}
      >
        Decrement
      </button>
      <input
        type="text"
        value={delta}
        onChange={event => setDelta(parseInt(event.target.value) || 0)}
      ></input>
      <button
        aria-label="Increment value"
        onClick={() => dispatch(logAndAdd(delta))}
      >
        Increment value by n
      </button>
    </div>
  )
}
