import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AppDispatch, RootGetState } from '../../app/store'

export interface CounterState {
  value: number
}

const initialState: CounterState = {
  value: 0,
}

export const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment: state => {
      state.value++
    },
    decrement: state => {
      state.value--
    },
    incrementByAmount: (state, action: PayloadAction<number>) => {
      state.value += action.payload
    },
  },
})

export const logAndAdd = (amount: number) => {
  return (dispatch: AppDispatch, getState: RootGetState) => {
    const stateBefore = getState()
    console.log(`Counter before: ${stateBefore.counter.value}`)
    dispatch(incrementByAmount(amount))
    const stateAfter = getState()
    console.log(`Counter after: ${stateAfter.counter.value}`)
  }
}

export const { increment, decrement, incrementByAmount } = counterSlice.actions
export const selectCount = (state: { counter: CounterState }) =>
  state.counter.value
export const counterReducer = counterSlice.reducer
