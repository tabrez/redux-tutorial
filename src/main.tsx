import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { App } from './App'
import { store } from './app/store'
import { Provider } from 'react-redux'

import './api/server'
import { ErrorBoundary } from './errorBoundary'

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ErrorBoundary>
        <App />
      </ErrorBoundary>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
)
