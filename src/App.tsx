import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Counter } from './features/counter/counter'
import { EditPost } from './features/posts/editPost'
import { NavBar } from './features/posts/navBar'
import { NewPost } from './features/posts/newPost'
import { PostsList } from './features/posts/postsList'
import { ShowPost } from './features/posts/showPost'

export const App = () => {
  return (
    <Router>
      <NavBar />
      <div className="App">
        <Switch>
          <Route exact path="/counter" component={Counter} />
          <Route exact path="/posts" component={PostsList} />
          <Route exact path="/new-post" component={NewPost} />
          <Route exact path="/posts/:postId/edit" component={EditPost} />
          <Route exact path="/posts/:postId" component={ShowPost} />
        </Switch>
      </div>
    </Router>
  )
}
